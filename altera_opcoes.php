<?php
require 'cabecalho.php';

?>
  <style type="text/css">
    body {
      background-color: #FFFFFF;
    }
    body > .grid {
      height: 60%;
    }
    .column {
      max-width: 700px;
    }
  </style>
  
<h2 class="ui medium icon cengter aligned header">
  <i class="pencil icon"></i>
  <div class="content">
    Alterar Presente
    <div class="sub header">Deseja alterar sua opção? Digite aqui somente o que for alterar</div>
  </div>
</h2>
  <div class="ui divider"></div>
<br>
<div class="ui center aligned grid">
  <div class="column">
      <form class="ui large form" action="db_update.php" method="post">
            <div class="ui stacked segment">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="pencil icon"></i>
                        <input type="text" name="opcao1" placeholder="Alterar Opção 1">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="pencil icon"></i>
                        <input type="text" name="opcao2" placeholder="Alterar Opção 2">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="pencil icon"></i>
                        <input type="text" name="opcao3" placeholder="Alterar Opção 3">
                    </div>
                </div>
                <div class="field">
                    <label>Alterar Comentário (opcional)</label>
                    <div class="ui input">                        
                        <textarea rows="2" name="comentario" placeholder="Escreva algo..."></textarea>
                    </div>
                </div>
                <input type="submit" value="Alterar" class="ui large teal submit button">
                <a href="index.php" class="ui large red button">Cancelar</a>
            </div>
        </form>
  </div>
</div>

<?php
include 'rodape.php';
?>

