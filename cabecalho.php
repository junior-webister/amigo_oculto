<?php
include("seguranca.php"); // Inclui o arquivo com o sistema de segurança
include 'funcoes.php';
protegePagina(); // Chama a função que protege a página
?>

<!DOCTYPE html>
<html>
<head>

  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <title>Amigo Secreto | 2016</title>
  <!--- Favicon -->
    <link rel="icon" href="images/favicon.png" />
    
  <!--- Site CSS -->
  <link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/semantic.css">
  <link rel="stylesheet" type="text/css" href="estilo.css">
  <link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/components/reset.css">
  <link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/components/site.css">
  <link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/components/grid.css">

  <!--- Component CSS -->
  <link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/components/menu.css">
  <link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/components/input.css">
  <link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/components/dropdown.css">
  <link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/components/icon.css">
  <link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/components/button.css">
  <link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/components/transition.css">

  <!--- Example Libs -->
  <link rel="stylesheet" type="text/css" href="bower_components/semantic/dist/components/popup.css">
  <script src="bower_components/semantic/examples/assets/library/jquery.min.js"></script>
<!--  <script src="bower_components/semantic/examples/assets/library/iframe-content.js"></script>-->
<!--  <script src="bower_components/semantic/examples/assets/show-examples.js"></script>-->
  <script type="text/javascript" src="bower_components/semantic/dist/components/popup.js"></script>

  <!--- Component JS -->
  <script type="text/javascript" src="bower_components/semantic/dist/components/transition.js"></script>
  <script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.js"></script>
  <script type="text/javascript" src="bower_components/semantic/dist/components/dropdown.js"></script>
  <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.0/jquery.min.js"></script>


  <!--- Example Javascript -->

</head>
<body>

<div class="ui inverted menu">
  <div class="header item">Lista de Presentes </div>
  <a href="index.php" class="teal item">Página Inicial</a>
  <a href="cadastrar.php" class="teal item">Cadastrar</a>
<!--  <a class="teal item">Alterar</a>-->
<!--  <div class="ui dropdown item">
    Dropdown
    <i class="dropdown icon"></i>
    <div class="menu">
      <div class="item">Action</div>
      <div class="item">Another Action</div>
      <div class="item">Something else here</div>
      <div class="divider"></div>
      <div class="item">Separated Link</div>
      <div class="divider"></div>
      <div class="item">One more separated link</div>
    </div>
  </div>-->
  <div class="right menu">
      <div class="item">
          <?php 
          $user = $_SESSION['usuarioLogin'];
          $sql = "SELECT * FROM usuarios WHERE usuario = '".$user."'";
          $consulta = mysqli_query($_SG['link'], $sql);
          $avatar = mysqli_fetch_array($consulta);
          echo '<img class="ui avatar image" src="'.$avatar['avatar'].'"> ';
          echo '&nbsp;&nbsp;<span>'.$_SESSION['usuarioNome'].'</span>';
          ?>
      </div>
    <div class="item">
      <div class="ui action left icon input">
        <i class="search icon"></i>
        <input type="text" placeholder="Pesquisar">
        <button class="ui inverted basic button">Buscar</button>
      </div>
    </div>        
        <a class="item" href="logout.php"><i class="sign out icon"></i>Sair</a>      
  </div>
</div>

