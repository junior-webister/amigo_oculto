
-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tempo de Geração: 06/11/2016 às 12:40:42
-- Versão do Servidor: 10.0.20-MariaDB-wsrep
-- Versão do PHP: 5.2.17

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Banco de Dados: `u915126445_lista`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `backup_user`
--

CREATE TABLE IF NOT EXISTS `backup_user` (
  `id` int(10) unsigned NOT NULL DEFAULT '0',
  `nome` varchar(100) CHARACTER SET utf8 NOT NULL,
  `usuario` varchar(50) CHARACTER SET utf8 NOT NULL,
  `senha` varchar(50) CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(150) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Extraindo dados da tabela `backup_user`
--

INSERT INTO `backup_user` (`id`, `nome`, `usuario`, `senha`, `avatar`) VALUES
(1, 'Junior Nascimento', 'jjunior', 'ejwkh24', 'images/avatar/jjunior.png'),
(2, 'Fernando Nascimento', 'fnascimento', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/t31.0-8/s960x960/13738333_1042893592461468_7912574715486177351_o.jpg');

-- --------------------------------------------------------

--
-- Estrutura da tabela `lista`
--

CREATE TABLE IF NOT EXISTS `lista` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `opcao1` varchar(100) CHARACTER SET utf8 NOT NULL,
  `opcao2` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `opcao3` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `descricao` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `usuario` varchar(45) COLLATE utf8_unicode_ci NOT NULL,
  `id_usuario` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Extraindo dados da tabela `lista`
--

INSERT INTO `lista` (`id`, `opcao1`, `opcao2`, `opcao3`, `descricao`, `usuario`, `id_usuario`) VALUES
(6, 'Camiseta do CoringÃ£o ', 'Pijama liso de inverno', 'Meia p/ jogar bola', 'Eu quero tudo isso, e se possivel na cor preta... a camiseta pode ser falsa da barraquinha que eu nÃ£o ligo', 'jjunior', 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) CHARACTER SET utf8 NOT NULL,
  `usuario` varchar(50) CHARACTER SET utf8 NOT NULL,
  `senha` varchar(50) CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(150) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=16 ;

--
-- Extraindo dados da tabela `usuarios`
--

INSERT INTO `usuarios` (`id`, `nome`, `usuario`, `senha`, `avatar`) VALUES
(1, 'Junior Nascimento', 'jjunior', 'ejwkh24', 'https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-9/14695423_1150759785005448_6997443789428337408_n.jpg?oh=3a04763b6a1018aca30e4ec21433f549&oe=588C8AD3'),
(2, 'Fernando Nascimento', 'fnascimento', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/t31.0-8/s960x960/13738333_1042893592461468_7912574715486177351_o.jpg'),
(3, 'Fabiana Santos', 'fsantos', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/t31.0-8/q82/s960x960/14700929_1186855708020062_304851692412730642_o.jpg'),
(4, 'Fernanda Raquel', 'fraquel', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-9/13512190_10210334526622228_493640512255306696_n.jpg?oh=6871df0d9769e1712364a6048dcb6b23&oe=588D996E'),
(5, 'Dinha', 'isantos', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-9/1380447_225383930956676_1996070881_n.jpg?oh=c3b2d2001ab8f64e9ed15f98a59d665a&oe=589616EF'),
(6, 'Zé Santos', 'jsantos', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-9/381291_106865316093738_352273897_n.jpg?oh=f6f8ff49f96b12987a07b69957500604&oe=588B7EA9'),
(7, 'Daniela Santos', 'dsantos', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-9/11667504_10204724427447419_7668906735849724660_n.jpg?oh=7f4af25e29f1570152ec01cd47db43dd&oe=58A8A053'),
(8, 'Gabi Fortuna', 'gfortuna', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-9/13557733_1747136315561948_7629526379684633263_n.jpg?oh=9b4b362740fecb6c20a175c4f6b50cd1&oe=589C7D63'),
(9, 'Vinicius Tadeu', 'vtadeu', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-9/13887140_529280900598410_4340488598000095268_n.jpg?oh=40f11c844603b1d14631f4c5f4457dbe&oe=589E05CF'),
(10, 'Cristiane Santos', 'csantos', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-9/14462960_10207152608962291_1235885786902621956_n.jpg?oh=2bc6de5b847613596178f924e8d13483&oe=58A5F6C8'),
(11, 'Rafaella Aparecida', 'raparecida', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-9/13932668_1295271590484867_6764530629938411769_n.jpg?oh=ed8a6d70ac236edd7a507dbade3bb207&oe=58A5899C'),
(12, 'Angela Santos', 'asantos', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-9/14199165_1066900063429363_815478268481971941_n.jpg?oh=fd944fc0a1bec99349f3a6fb64435150&oe=589F7E07'),
(13, 'Gabriel Alvim', 'galvim', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-9/12096591_861749687245668_898019221780626453_n.jpg?oh=ea48ad1da5fa2c0d1deb39783ad16db6&oe=58A015EF'),
(14, 'Raphael Alvim', 'ralvim', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/v/t1.0-9/14716083_912080575564078_7613822885311470243_n.jpg?oh=b46e8b74e429a4eba497ba1ce59e288f&oe=58A270C9'),
(15, 'Juliano Camargo', 'jcamargo', 'amigo2016', 'https://scontent.fgru5-1.fna.fbcdn.net/v/l/t1.0-9/12105971_1236105696415178_403927708604372613_n.jpg?oh=6452bb058d27b337557437685fda2b7f&oe=5892197A');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
