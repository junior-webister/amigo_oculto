<?php
include 'cabecalho.php';

$senha = mysqli_query($_SG['link'],"SELECT * FROM usuarios WHERE id = ".$_SESSION['usuarioID']);
$primeiro_acesso = mysqli_fetch_assoc($senha);

if ($primeiro_acesso['senha'] == 'amigo2016') {
    echo '<br><br>'
            . '<div class="ui center aligned grid">'
            . '<div class="ui attached message">'
            . '<div class="header">'
            . 'Atenção!</div>'
            . '<p>Você ainda não trocou a senha padrão <i>amigo2016</i> , por favor informe uma nova senha abaixo!</p>'
            . '<form method="post" class="ui form attached fluid segment" action="troca_senha.php">'
            . '<div class="field">'
            . '<label>Nova senha</label>'
            . '<input placeholder="senha" type="password" name="senha">'
            . '</div>'
            . '<input type="submit" class="ui teal submit button"></div>'
            . '</form>'
            . '</div>';
            exit();
}

?>
  <style type="text/css">
    body {
      background-color: #FFF;
    }
    body > .grid {
      height: 60%;
      
    }
    .table {
        padding: 0px;
    }
    .column {
      max-width: 85%;
    }
  </style>
  
<h2 class="ui medium icon cengter aligned header">
  <i class="unordered list icon"></i>
  <div class="content">
    Lista de Presentes
    <div class="sub header">Veja a lista de presentes e confira se o seu amigo já cadastrou!</div>
  </div>
</h2>
  <div class="ui divider"></div>
  <br>
  
  <?php
    $usuario_nome = $_SESSION['usuarioNome'];
    $sql2 = mysqli_query($_SG['link'], "
      select l.opcao1
	,l.opcao2
        ,l.opcao3
        ,l.descricao
        ,u.nome 
        ,u.avatar
        ,l.id
    from 
	lista as l
    inner join 
	usuarios as u 
    on
	l.id_usuario = u.id
    order by 5 
    ;");
    
	echo "<table class='ui center aligned collapsing celled table'>";
        
	echo "<thead>";
		echo "<th>Nome</th>";
		echo "<th>Opção 1</th>";
                echo "<th>Opção 2</th>";
                echo "<th>Opção 3</th>";
                echo "<th>Comentário</th>";
                echo "<th>Ações</th>";
	echo "</thead>";
        
        while ($res=mysqli_fetch_array($sql2)) {
		echo "<tr>";
		echo "<td class='left aligned middle aligned' width='18%'>";
                echo '<h4 class="ui image header">';
                echo '<img src="'.$res['avatar'].'" class="ui mini rounded image">';
                echo '<div class="content">';
                echo $res['nome'];
                echo '</div>';
                echo '</h4>';
		echo "</td>";
                echo "<td class='middle aligned'>";
                echo utf8_encode($res['opcao1']);
		echo "</td>";
                echo "<td class='middle aligned'>";
                echo $res['opcao2'];
		echo "</td>";
                echo "<td class='middle aligned'>";
                echo $res['opcao3'];
		echo "</td>";
                echo "<td class='middle aligned'>";
                echo $res['descricao'];
		echo "</td>";
                echo "<td class='middle aligned'>";
                if ($usuario_nome == $res['nome']) {
                    echo "<a href='altera_opcoes.php' class='compact ui mini teal button'><i class='pencil icon'></i>Alterar</a>";
                    echo "<a href='#popup1' class='compact ui mini red button'><i class='trash icon'></i>Excluir</a>";
                    //echo "<a href='' class='compact ui mini red button'><i class='trash icon'></i>Excluir</a>";
                } else {
                    echo "-";
                }
		echo "</td>";
		echo "</tr>";
        }
        echo "</table>";
        
  ?>
          

<?php
include 'popups.php';
include 'rodape.php';
?>