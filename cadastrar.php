<?php
require 'cabecalho.php';
?>
  <style type="text/css">
    body {
      background-color: #FFFFFF;
    }
    body > .grid {
      height: 60%;
    }
    .column {
      max-width: 700px;
    }
  </style>
  
<h2 class="ui medium icon cengter aligned header">
  <i class="gift icon"></i>
  <div class="content">
    Cadastrar Presente
    <div class="sub header">Insira suas opções. Se preferir, escreva algum comentário para facilitar</div>
  </div>
</h2>
  <div class="ui divider"></div>
<br>
<div class="ui center aligned grid">
  <div class="column">
        <form class="ui large form" action="db_opcoes.php" method="post">
            <div class="ui stacked segment">
                <div class="field">
                    <div class="ui left icon input">
                        <i class="gift icon"></i>
                        <input type="text" name="opcao1" placeholder="Opção 1">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="gift icon"></i>
                        <input type="text" name="opcao2" placeholder="Opção 2">
                    </div>
                </div>
                <div class="field">
                    <div class="ui left icon input">
                        <i class="gift icon"></i>
                        <input type="text" name="opcao3" placeholder="Opção 3">
                    </div>
                </div>
                <div class="field">
                    <label>Comentário (opcional)</label>
                    <div class="ui input">                        
                        <textarea rows="2" name="comentario" placeholder="Escreva algo..."></textarea>
                    </div>
                </div>
                <input type="submit" value="Cadastrar" class="ui large teal submit button">
            </div>
        </form>
  </div>
</div>

<?php
include 'rodape.php';
?>